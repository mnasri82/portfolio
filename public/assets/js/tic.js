require("../css/style2.scss");

//Déclaration de fonction bouton qui verifie si le bouton est vide ou pas
function bouton(button)
{
    // Version COOOOOOOL
    // De vérifier non pas que inner HTML soit à 0 en longueur mais :
    // De vérifier que la data joueur soit à "0" 
    // veérifier que button.dataset.joueur === 0
    return button.dataset.joueur === '0';
}
//  Deux images, une pour le 'x' et l'autre pour le 'o'
var image1 = '<img src="./assets/img/cross-2.png" width="100" />';
var image2 = '<img src="./assets/img/circle-2.png" width="100" />';

// Initialement, c'est le joueur 1 qui commence
var joueur1 = true;
 

function setSymbol(btn, symbole){
    var img;
    if(symbole === 'X'){
        img = image1
    }
    // Si symbole === X
        // alors img = image1
        // et dataset de btn est egal à X
    // sinon img = image2
    else{
        img = image2
    }
    btn.dataset.joueur = symbole;

 


    // Symbole ici va être une image.... '<img src="...."' />' qui va dépendre de symbole
    // Si symbole vaut X alors image 1
    // Sinon image 2
     btn.innerHTML = img
}     
/**
 * 8 combinaisons pour gagner
 * 
 * @param array pions 
 * @param array joueurs 
 * @param int   tour 
 */
function rechercherVainqueur(pions, joueurs, tour)
{
      // Première Ligne ok ?
      if (pions[0].dataset.joueur == joueurs[tour] &&
         pions[1].dataset.joueur == joueurs[tour] &&
         pions[2].dataset.joueur == joueurs[tour])
      {
        pions[0].style.backgroundColor = "blue";
        pions[1].style.backgroundColor = "blue";
        pions[2].style.backgroundColor = "blue";
        return true;
      }

      // Deuxième Ligne OK ?
      if (pions[3].dataset.joueur == joueurs[tour] &&
          pions[4].dataset.joueur == joueurs[tour] &&
          pions[5].dataset.joueur == joueurs[tour])
      {
        pions[3].style.backgroundColor = "blue";
        pions[4].style.backgroundColor = "blue";
        pions[5].style.backgroundColor = "blue";
        return true;
      }
      // Troisième Ligne OK ?
      if (pions[6].dataset.joueur == joueurs[tour] &&
          pions[7].dataset.joueur == joueurs[tour] &&
          pions[8].dataset.joueur == joueurs[tour])
      {
        pions[6].style.backgroundColor = "blue";
        pions[7].style.backgroundColor = "blue";
        pions[8].style.backgroundColor = "blue";
        return true;
      }
        //Première Colonne OK ?
      if (pions[0].dataset.joueur == joueurs[tour] &&
          pions[3].dataset.joueur == joueurs[tour] &&
          pions[6].dataset.joueur == joueurs[tour])
      {
        pions[0].style.backgroundColor = "blue";
        pions[3].style.backgroundColor = "blue";
        pions[6].style.backgroundColor = "blue";
        return true;
      }
          //Deuxième Colonne OK ?
      if (pions[1].dataset.joueur == joueurs[tour] &&
          pions[4].dataset.joueur == joueurs[tour] &&
          pions[7].dataset.joueur == joueurs[tour])
      {
        pions[1].style.backgroundColor = "blue";
        pions[4].style.backgroundColor = "blue";
        pions[7].style.backgroundColor = "blue";
        return true;
      }
          // Troisième Colonne OK ?
      if (pions[2].dataset.joueur == joueurs[tour] &&
          pions[5].dataset.joueur == joueurs[tour] &&
          pions[8].dataset.joueur == joueurs[tour])
      {
        pions[2].style.backgroundColor = "blue";
        pions[5].style.backgroundColor = "blue";
        pions[8].style.backgroundColor = "blue";
        return true;
      }
          // Première Diagonale OK ?
      if (pions[0].dataset.joueur == joueurs[tour] &&
          pions[4].dataset.joueur == joueurs[tour] &&
          pions[8].dataset.joueur == joueurs[tour])
      {
        pions[0].style.backgroundColor = "blue";
        pions[4].style.backgroundColor = "blue";
        pions[8].style.backgroundColor = "blue";
        return true;
      }
          // Deuxième Diagonale OK ?
      if (pions[2].dataset.joueur == joueurs[tour] &&
          pions[4].dataset.joueur == joueurs[tour] &&
          pions[6].dataset.joueur == joueurs[tour])
      {
        pions[2].style.backgroundColor = "blue";
        pions[4].style.backgroundColor = "blue";
        pions[6].style.backgroundColor = "blue";
        return true;
      }
}

function matchNul(pions)
{
    // De 0 à 8 (longueur de tableau pions - 1 (on commence à 0))
     for (var i = 0, len = pions.length; i < len; i++)
     {
        // Si le inner HTML du pion est vide alors on retourne faux
        // Soit : si une case est vide alors c'est que ce n'est pas fini et qu'on n'a pas match Null !!
        // Dans ce cas on s'arrête
        // VERSION COOOOOOL
        // Vérifier que data-joueur === '0'
        if (pions[i].dataset.joueur === '0')
              return false;
     }

     return true;
}

 

/**
 * 
 */
class Afficheur {

    constructor( element ) {
        this.affichage = element;
    }

    sendMessage( message ) {
        this.affichage.innerHTML = message;
    }
}
/**
 * Ma fonction principale, celle qui lance le jeu
 */
function main()
{
     //Déclaration de fonction et variables avec parametres du jeu :tableau avec joueur,valeur 0 pour tour ,valeur false pour la variable "over"  
     var pions = document.querySelectorAll("#Jeu button");
     var joueurs = ['X', 'O'];
     var tour = 0;
     var over = false;
     //Déclaration de classe on crée une instance de Afficheur
     var afficheur = new Afficheur(document.querySelector("#StatutJeu"));
     // On affiche que le jeu peut commencer
     afficheur.sendMessage("Le jeu peut commencer ! <br /> Joueur " + joueurs[tour] + " c'est votre tour.");
     
     // Pour chaque case du jeu....
     for (var i = 0, len = pions.length; i < len; i++)
     {
        // Ecoute sur chaque boutons (chaque case) si l'évènement click se produit
        // Pour la case pions[i] on ajoute un event listener (un écouteur de l'événement click)
         pions[i].addEventListener("click", function()
         {
            // Si over est vrai, alors on s'arrête
            // Over est vrai quand c'est la fin du jeu
            // Au début over étant faux, on ne passe pas ici
            if (over) {
                return;
            }

              //  ! = à not ,this correspond à afficheur,si je click sur case occupée alors  message affiché
              // Bouton renvoie vrai ou faux 
              // Si bouton renvoie faux, alors la case est occupée
              // Dans ce cas, !bouton(this) === true => bouton(this) === false (this === pions[i])
              // if( bouton(pions[i]) === false )
              if (!bouton(this))
              {
                  //fonction afficheur message
                  afficheur.sendMessage("Case occupée ! <br />Joueur " + joueurs[tour] + " c'est toujours à vous !");

              }
              else
              {
                  //setSymbol renvoie le symbole des joueurs et le vainqueur est affiché  
                  // Modifier la case avec le bon symbole
                  // (modifie les boutons html en symbole)
                  setSymbol(this, joueurs[tour]);

                  // On regarde s'il y a un vainqueur pour les pions, les joueur et le tour en cours 
                  over = rechercherVainqueur(pions, joueurs, tour);

                  // Si c'est terminé
                  if(over)
                  {
                        // On affiche le vainqueur et on s'arrête
                        afficheur.sendMessage("Le joueur " + joueurs[tour] + " a gagné ! <br /> <a href='/projet2'>Rejouer</a>");
                        return;
                  }
                  // Si Match Null Condition
                  if (matchNul(pions))
                  {
                        // On l'affiche et on s'arrete
                        afficheur.sendMessage("Match Nul ! <br/> <a href='/projet2'>Rejouer</a>");
                        return;
                  }

                  // Sinon on inverse le tour
                  // Si tour === 0 => tour = 1
                  // Sinon => tour = 0
                  // tour = ( tour === 0 ) ? 1 : 0;
                  tour++;
                  tour = tour % 2;
                  
                  // Affiche le joueur qui doit jouer
                  afficheur.sendMessage("Joueur " + joueurs[tour] + " c'est à vous !");
              }
         });
     }
}
    document.addEventListener('DOMContentLoaded', main);

  