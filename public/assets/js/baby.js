require("../css/boots.scss");
function initAudio(){
   
    // lancement de l'audio happy.
    let audio =new Audio("./assets/sounds/happy.ogg");
    audio.play();
    
    
  }
  
  let audio1 = new Audio("./assets/sounds/cry.ogg");
  //déclaration de variable  
  
  //déclaration des variables en rapport avec mes Id.
  var humeur = document.getElementById("btnmanger ");
  var humeur2 = document.getElementById("btnchanger");
  var humeur3 = document.getElementById("btnjouer");
  var ibaby = document.getElementById('ibaby');
  //déclaration de variable 
  var pensee = 0;
  let bonneReponse = false;
  let imgBaby = document.getElementById('obaby');
  
    let Over = document.getElementById('Over');
    let  gameOver = setTimeout (isItOver, 10000);
   
  /**
   *  fonction qui détermine les pensées du bébé en aléatoire
   */
  function pensee_du_bebe (){
    setTimeout(function() {
      ibaby.setAttribute("src", './assets/img/cry.png');
      audio1.play();
      pensee = Math.floor(Math.random() * 3);
      
      switch (pensee){
        case 0:
          imgBaby.setAttribute("src", './assets/img/hamburger.png');
        break;
        
        case 1:
         imgBaby.setAttribute("src", '/assets/img/poo.png');
        break;
  
        case 2:
          imgBaby.setAttribute("src", './assets/img/intero.png');
        break;
  
        default:
        break;
  
      } 
         
      
    }, 5000);
  }
  
  
  
  
  
  /**
   * Mon bébé est-il satisfait ?
   * Si oui alors smile
   * Sinon cry...
   */
  function manger(){
    if (pensee  === 0) { // pensee DOIT être un entier egal à 1
      // je dois arreter le timer
      clearTimeout(gameOver);
      gameOver = setTimeout (isItOver, 10000);
      ibaby.setAttribute('src', "./assets/img/smile.png ");
      pensee_du_bebe();
       
    }
    else {
      // Sinon le timer continue
      // ibaby.setAttribute('src',"./assets/img/cry.png");
       
    }
  }; 
  
  /**
   * Mon bébé doit-il être changé ?
   * Si oui alors smile, 
   * Sinon, cry !
   */
  function changer(){
    if (pensee  === 1) {
      ibaby.setAttribute('src', "./assets/img/smile.png ");
      pensee_du_bebe();
      clearTimeout(gameOver);
      gameOver = setTimeout (isItOver, 10000);
    }
    else {
      // ibaby.setAttribute('src',"./assets/img/cry.png");
  
      
    }
  }; 
  
  function jouer(){
    if (pensee  === 2) {
      ibaby.setAttribute('src', "./assets/img/smile.png ");
      pensee_du_bebe();
      clearTimeout(gameOver);
      gameOver = setTimeout (isItOver, 10000);
  
    }
    else {
      // ibaby.setAttribute('src',"./assets/img/cry.png");
      
       
    }
  }; 
  
  /**
   * Si le bébé pleure plus de X secondes, alors gameover
   * Sinon on continue de jouer...
   */
  function isItOver (){
    let laSourceDeMonImage = ibaby.getAttribute("src");
     
    
  
    // Si laSourceDeMonImage contient smile alors on continue
    if (laSourceDeMonImage.indexOf('smile') !== -1){
      // Alors on continue de jouer en aléatoire 
      
    }
    // Sinon, gameover
    else {
       
      
      // C'est terminé pour toi !! Gameover !!
        gameOver = setTimeout (isItOver, 10000);
        Over.style.display = "block";
        imgBaby.style.visibility="hidden";
    }
  }     
        
  
  
  
  
  
  
  document.getElementById('btnmanger').addEventListener('click', manger);
  
   
  document.getElementById('btnchanger').addEventListener('click', changer);
  
  
  document.getElementById('btnjouer').addEventListener('click', jouer);
  
  
  window.addEventListener('load', pensee_du_bebe);
  window.addEventListener('load', initAudio);
  
       