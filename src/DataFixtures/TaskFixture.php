<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Task;

class TaskFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
       

    for ($i = 0; $i < 3; $i++) {
        $task = new Task();
        $task->setDescription('Ma mission est '.$i);
        $task->setDone(false);
        $manager->persist($task);
    }

        $manager->flush();

    }

}




