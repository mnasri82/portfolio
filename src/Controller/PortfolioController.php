<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PortfolioController extends AbstractController
{
    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function index()
    {
        return $this->render('portfolio/index.html.twig', [
            'controller_name' => 'PortfolioController',
        ]);
    }


 /**
     * @Route("/projet1", name="projet1")
     */
    public function projet1()
    {
        return $this->render('portfolio/projet1.html.twig', [
            'controller_name' => 'PortfolioController',
        ]);
    }




 /**
     * @Route("/projet2", name="projet2")
     */
    public function projets2()
    {
        return $this->render('portfolio/projet2.html.twig', [
            'controller_name' => 'PortfolioController',
        ]);
    }

    

/**
     * @Route("/cv", name="cv")
     */
    public function cv()
    {
        return $this->render('portfolio/cv.html.twig', [
            'controller_name' => 'PortfolioController',
        ]);
    }







}





