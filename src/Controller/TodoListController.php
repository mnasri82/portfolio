<?php

namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use symfony\component\httpfoundation\RedirectResponse;


class TodoListController extends AbstractController
{
    /**
     * @Route("/todo_list", name="todo_list")
     */
    public function index()
    {
        $tasks=$this->getDoctrine()->getRepository(Task::class)->findBy([],['id'=>'DESC']);
        return $this->render('portfolio/home.html.twig', ['tasks'=>$tasks]);
    }

     /**
     * @Route("/home", name="home")
     */
    public function home(){

        return $this->render('portfolio/home.html.twig');
    
    }

    /**
     * @Route("/create", name="create_task" , methods={"POST"}) 
    */   
    public function Create(Request $request){

        $title=trim($request->request->get('title'));

    if(empty($title))

        return $this->redirectToRoute('todo_list');

    $entityManager = $this->getDoctrine()->getManager();

    $task = new Task;

    $task->setTitle($title);

    $entityManager->persist($task);

    $entityManager->flush();

        return $this->redirectToRoute('todo_list');
   
    }
    /**
     * @Route("/switch-status/{id}", name="switch_status") 
     */   
    public function switchstatus($id){

        $entityManager = $this->getDoctrine()->getManager();
        $task= $entityManager->getRepository(Task::class)->Find($id);

        $task->setStatus(! $task->getStatus());

        $entityManager->flush();

        return $this->redirectToRoute('todo_list');
    
   
    } 
    /**
    * @Route("/delete/{id}", name="task_delete") 
    */   
    public function delete(Task $id){

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($id);

        $entityManager->flush();

        return $this->redirectToRoute('todo_list');
    }





}    